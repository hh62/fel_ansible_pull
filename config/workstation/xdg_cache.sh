if [[ -d /xtmp && -w /xtmp && -n "$LOGNAME" ]]
then
  if [[ -d /xtmp/${LOGNAME} && -w /xtmp/${LOGNAME} ]]
  then
    XDG_CACHE_HOME=/xtmp/${LOGNAME} 
  else
    /bin/mkdir /xtmp/${LOGNAME} 
    /bin/chmod 0700 /xtmp/${LOGNAME} 
    XDG_CACHE_HOME=/xtmp/${LOGNAME} 
  fi
  # set this for ccache here
  CCACHE_DIR=$XDG_CACHE_HOME
fi
export XDG_CACHE_HOME CCACHE_DIR
